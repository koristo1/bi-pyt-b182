#!/bin/env/ python3


import re

# TODO load encoding

# TODO go through cs.wl and for each record check the part after '/'
# TODO if none exists, just print/save the word
# TODO construct all combinations of prefixes and suffixes
DELIMITER = "/"

with open("cs/cs.wl", "r", encoding="iso-8859-2") as WORDS:
    line = WORDS.readline()
    print(line, end="")
    while not DELIMITER in line:
        line = WORDS.readline()
        print(line, end="")
    split_line = line.split(sep=DELIMITER)
    text = split_line[0]
    regexp = split_line[1]
    print(regexp)
    for char in regexp:
        print(char)
        regex = re.compile('^[PS]FX ' + char)
        with open("cs/cs_affix.dat", "r", "iso-8859-2") as affices:
            for affix_line in affices:
                result = regex.search(line)
