import matplotlib.pyplot as plt

with open('sequence.fasta') as FILE:
    genome = ''.join(FILE.read().split('\n')[1:])
print(len(genome))

plt.axis([0, 5000000, 0, 5000000])
for i in range(0, len(genome), 100_000):
    print(i)
    sub = genome[i:i + 100_000]
    y = (sub.count('C') + sub.count("G")) / 100_000
    plt.plot(i - 5000, y, 'g+')
plt.show()
