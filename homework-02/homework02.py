#!/usr/bin/env Python3

from random import choice

import matplotlib.pyplot as plt

# CONSTANTS
HEIGHT = 100
WIDTH = 100
MOVE_CHOICES = (-1, 0, 1)

# DEBUG
SHOW_AXES = True
INITIAL_RGB_CHANNELS = 255
DEBUG_MOVEMENT = False


# create a grid that holds the hue of every vertex
def initialise_grid():
    return [[INITIAL_RGB_CHANNELS for x in range(WIDTH)] for y in range(HEIGHT)]


def print_grid(grid):
    for row in grid:
        print(row)


def update_vertex(grid, x, y):
    return grid[y][x] + (0 if grid[y][x] == 0 else -0.1)


def collect_vertices(grid):
    vertices = [[], [], []]
    for y in range(HEIGHT):
        for x in range(WIDTH):
            if grid[y][x] != 1:
                vertices[0].append(x)
                vertices[1].append(y)
                vertices[2].append(grid[y][x])
    return vertices


def display_plot(grid):
    vertices = collect_vertices(grid)
    plt.scatter(vertices[0], vertices[1], marker=",", c=vertices[2])
    plt.axis([0, WIDTH, 0, HEIGHT]) if SHOW_AXES else plt.axis('off')
    plt.gray()
    plt.show()


def place_starting_token(grid, x=WIDTH // 2, y=HEIGHT // 2):
    grid[y][x] = update_vertex(grid, x % WIDTH, y % HEIGHT)


def move():
    return choice(MOVE_CHOICES)


def main():
    x, y = WIDTH // 2, HEIGHT // 2
    # create a grid that holds the hue of every vertex
    grid = initialise_grid()
    place_starting_token(grid, x, y)

    while WIDTH - 1 >= x >= 0 and HEIGHT - 1 >= y >= 0:
        grid[y][x] = update_vertex(grid,x,y)
        if DEBUG_MOVEMENT:
            print(x, y, end='->')
        x += move()
        y += move()
        if DEBUG_MOVEMENT:
            print(x, y)

    display_plot(grid)


main()
