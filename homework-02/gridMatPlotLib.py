#!/usr/bin/env python3


import matplotlib.pyplot as plt
DIMENSION = 10

VERTICES = [[],[]]

for x in range(1, DIMENSION+1):
	for y in range(1, DIMENSION+1):
		VERTICES[0].append(x)
		VERTICES[1].append(y)

plt.plot(
	VERTICES[0],
	VERTICES[1],
	'ro'	
	)
plt.axis([0,DIMENSION+1,0,DIMENSION+1])
plt.axis('off')
plt.show()


print(VERTICES[0])
print(VERTICES[1])
