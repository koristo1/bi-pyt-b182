# /usr/bin/env python3
TAB = '\t'

with open("aminokyseliny.csv", "r") as INPUT, open("codons.py", 'w') as OUTPUT:
    OUTPUT.write("codons ={\n")
    for line in INPUT:
        name, mark, shortcut, codons = line.strip().split(TAB)
        if name.endswith("*"):
            continue
        codons = codons.split(", ")
        for codon in codons:
            OUTPUT.write(' "' + codon + '" :"' + mark + '",\n')

    OUTPUT.write("}")
