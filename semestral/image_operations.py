import numpy as np

from config import LEFT, VERTICAL, GREYSCALE_FORMULA


def mirror(image, alignment):
    """
    Flips the numpy array 'image' by the 'alignment' axis
    :param image: numpy array image to be flipped
    :param alignment: direction of flipping
    :return: numpy array representing a flipped image
    """
    return np.flipud(image) if alignment == VERTICAL else np.fliplr(image)


def invert(image):
    """
    Inverts the colours of the 'image'
    :param image: numpy array representing an image to be colour inverted
    :return: numpy array representing a inverted image
    """
    return 255 - image


def rotate(image, direction):
    """
    Rotates the 'image' array by 90 degrees in 'direction' - either clockwise or counterclockwise
    :param image: numpy array representing an image to be rotated
    :param direction: direction of the rotation
    :return: numpy array representing a rotated image
    """
    return np.rot90(image, 1 if direction == LEFT else 3)


def dim(image):
    """
    Dims the 'image' array
    :param image: numpy array representing an image to be dimmed
    :return: numpy array representing a dimmed image
    """
    image[...] = image[...] * .75
    return image


def brighten(image):
    """
    Brightens all the channels of the image represented by the 'image' numpy array
    :param image:numpy array representing an image to be brightened
    :return:numpy array representing a brightened image
    """
    image[...] = image[...] + (256 - image[...]) * 0.25
    return image


def greyscale(image):
    """
    Turns the image represented by the 'image' array to shades of grey
    :param image: numpy array representing image to turn into shades of grey
    :return: numpy array representing a greyscaled image
    """
    return np.sum(GREYSCALE_FORMULA * image, axis=2)


def highlight(image):
    """
    Takes a picture and highlights its edges - takes into account all surrounding cells
    :param image: Image to be highlighted
    :return: numpy array representing  a sharpened image
    """
    copy = np.copy(image)

    width, height, channels = image.shape
    for x in range(1, width - 1):
        for y in range(1, height - 1):
            for z in range(channels):
                channel = (
                        + 9 * image[x, y, z]
                        - image[x - 1, y, z]
                        - image[x + 1, y, z]
                        - image[x, y + 1, z]
                        - image[x, y - 1, z]
                        - image[x - 1, y + 1, z]
                        - image[x - 1, y - 1, z]
                        - image[x + 1, y - 1, z]
                        - image[x + 1, y + 1, z])
                if channel < 0:
                    channel = 0
                if channel > 255:
                    channel = 255
                copy[x, y, z] = channel
    return copy
