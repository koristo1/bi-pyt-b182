import tkinter as tk
import tkinter.filedialog as tkFileDialog

import numpy as np
from PIL import Image, ImageTk

from config import LEFT, RIGHT, HORIZONTAL, VERTICAL
from image_operations import rotate, brighten, dim, mirror, invert, greyscale, highlight


def refresh(image_array, image_tray):
    """
    Replaces the image in 'image_tray' by the image constructed from 'image_array'
    :param image_array: numpy array representation of the new image
    :param image_tray: container containing the current image
    :return: none
    """
    global img_array
    image = Image.fromarray(image_array).convert("RGB")
    tk_img = ImageTk.PhotoImage(image)
    image_tray.configure(image=tk_img)
    image_tray.image = tk_img
    img_array = np.array(image)


def save():
    """
    Prompts the user to save the file
    :return: none
    """
    global img_array, root
    image = Image.fromarray(img_array).convert("RGB")
    file = tkFileDialog.asksaveasfilename(title="Save as",
                                          filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))
    image.save(file)


def open():
    """
    Prompts the user to select an image to load into the programme
    :return: none
    """
    file = tkFileDialog.askopenfilename(title="Select file",
                                        filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))
    return Image.open(file)


def add_buttons():
    button_right = tk.Button(frame, text="Rotate clockwise", fg="red",
                             command=lambda: refresh(rotate(img_array, RIGHT), img_frame))
    button_left = tk.Button(frame, text="Rotate counterclockwise", fg="red",
                            command=lambda: refresh(rotate(img_array, LEFT), img_frame))
    button_bright = tk.Button(frame, text="Brighten", fg="red",
                              command=lambda: refresh(brighten(img_array), img_frame))
    button_dim = tk.Button(frame, text="Dim", fg="red",
                           command=lambda: refresh(dim(img_array), img_frame))
    button_grey = tk.Button(frame, text="Grey scale", fg="red",
                            command=lambda: refresh(greyscale(img_array), img_frame))
    button_highlight = tk.Button(frame, text="Sharpen edges", fg="red",
                                 command=lambda: refresh(highlight(img_array), img_frame))
    button_vertical = tk.Button(frame, text="Flip vertically", fg="red",
                                command=lambda: refresh(mirror(img_array, VERTICAL), img_frame))
    button_horizontal = tk.Button(frame, text="Flip horizontally", fg="red",
                                  command=lambda: refresh(mirror(img_array, HORIZONTAL), img_frame))
    button_invert = tk.Button(frame, text="Invert", fg="red",
                              command=lambda: refresh(invert(img_array), img_frame))
    button_save = tk.Button(frame, text="Save", fg="red", command=lambda: save())
    button_right.pack(side=tk.LEFT)
    button_left.pack(side=tk.LEFT)
    button_bright.pack(side=tk.LEFT)
    button_dim.pack(side=tk.LEFT)
    button_grey.pack(side=tk.LEFT)
    button_highlight.pack(side=tk.LEFT)
    button_vertical.pack(side=tk.LEFT)
    button_horizontal.pack(side=tk.LEFT)
    button_invert.pack(side=tk.LEFT)
    button_save.pack(side=tk.LEFT)


def add_image_tray():
    global img_array, img_frame
    img = open()
    img_array = np.array(img)
    img = Image.fromarray(img_array)
    tk_img = ImageTk.PhotoImage(img)
    img_frame = tk.Label(root, image=tk_img, bg='white')
    img_frame.pack(side=tk.TOP)
    refresh(img_array, img_frame)


def set_root():
    global root, frame
    root = tk.Tk()
    frame = tk.Frame(root)
    frame.pack()


set_root()
add_image_tray()
add_buttons()
root.mainloop()
