import numpy as np
import matplotlib.pyplot as plt


# Plotting function creates a heatmap
def plot(values, tolerance, x_min, x_max, y_min, y_max):
    plt.xlabel("Real")
    plt.ylabel("Imaginary")
    plt.title("The Mandelbrot Set. Iterations: %s" % tolerance)
    plt.imshow(values, origin="0,0", interpolation="none", extent=[x_min, x_max, y_min, y_max])
    plt.show()


# Algorithm to compute if point is within set
def calculation(point, iteration):
    z = 0
    # number of steps before abs of the complex number reaches 2
    for i in range(1, iteration):
        if abs(z) > 2:
            return i
        z = z * z + point
    return 0


# Main function intialises & creates grid.
def main(re_min, re_max, im_min, im_max, grid_length, iterations):
    real = np.linspace(re_min, re_max, grid_length)  # real values
    imag = np.linspace(im_min, im_max, grid_length)  # imaginary values
    values = np.empty((grid_length, grid_length))  # grid

    for i in range(grid_length):  # go through the grid
        for j in range(grid_length):
            values[j, i] = calculation(real[i] + 1j * imag[j], iterations)  # save the calculation
    plot(values, iterations, re_min, re_max, im_min, im_max)  # plot

    return


print('Computing set ...')
main(-2, 2, -2, 2, 1000, 15)
print("Set computed")
