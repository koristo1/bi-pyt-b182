#!/usr/bin/env Python3

from shutil import get_terminal_size
from functools import partial
from random import randint
from random import choice
from time import sleep

pp = partial(print, end='', flush=True)

COLUMNS, ROWS = get_terminal_size()

#-----------------------------------
#CONTROLS
TOKEN_COUNTER = (ROWS*COLUMNS)//((ROWS+COLUMNS))
DIMENSIONS = 2
GRACE_PERIOD = 0.075
REDRAW_WALL_PERIOD = 10
TAIL_TOGGLE = False
START_FREEZE = False
CORNER_DEBUG = False
COLLISION_DEBUG = False
#------------------------------------
#Randomisers
MOVE_CHOICE = (-1,0,1)
CORNER_TAIL_CHOICE = (0,1)
STARTING_TAIL_OFFSET =(-1,1)

borders=[]
vectors=[]

#------------------------------------
#TERMINAL CLEAR
pp('\033[2J')

#------------------------------------
#BORDERS GENERATION
for x in (1,COLUMNS): #vertical
	for y in range(1,ROWS):
		borders.append([x,y])
		
for y in (1,ROWS): #horizontal walls
	for x in range(1,COLUMNS+1):
		borders.append([x,y])
#------------------------------------
#PRINT SCREEN BORDER
for coordinates in borders:
	pp(f'\033[{coordinates[1]};{coordinates[0]}H\033[42m '.format(coordinates[1], coordinates[0]))
pp(f'\033[{coordinates[1]};{coordinates[0]}H\u001b[0m'.format(1,1))
#------------------------------------
#CREATE TOKENS
#[HEAD,TAIL] = [[row,column],[row,column]]
for token in range(TOKEN_COUNTER): #GENERATE RANDOM BOUND COORDINATES
	x,y = randint(2,COLUMNS-1),randint(2,ROWS-1)
	previousX, previousY = x + choice(STARTING_TAIL_OFFSET), y + choice(STARTING_TAIL_OFFSET)
	vectors.append([[x,y],[previousX, previousY]])


#-----------------------------------
#CORNER DEBUGGING MOLECULES
if CORNER_DEBUG:
	vectors.append([[4,4],[5,5]]) #top left corner
	vectors.append([[COLUMNS-3,4],[COLUMNS-4,5]]) #top right corner
	vectors.append([[4,ROWS-3],[5,ROWS-4]]) #bottom left corner
	vectors.append([[3,3],[4,3]])
	vectors.append([[COLUMNS-3,ROWS-3],[COLUMNS-4,ROWS-3]])
	vectors.append([[COLUMNS-3,4],[COLUMNS-4,5]])
	vectors.append([[COLUMNS-4,4],[COLUMNS-5,5]])
	vectors.append([[5, ROWS-3],[6,ROWS-4]])	
	vectors.append([[5,3],[5,4]]) #top wall
	vectors.append([[COLUMNS-3,ROWS-2],[COLUMNS-3,ROWS-3]]) #bottom wall

#-----------------------------------
#COLLISION DEBUGGING MOLECULES
if COLLISION_DEBUG:
	vectors.append([[10,10],[9,10]])
	vectors.append([[11,10],[12,10]])
	vectors.append([[20,20],[19,19]])
	vectors.append([[20,21],[19,21]])

#-----------------------------------
#DRAW INITIAL TOKENS
for vector in vectors:
	pp('\033[{};{}HO'.format(vector[0][1], vector[0][0]))
	if TAIL_TOGGLE:
		pp('\033[{};{}Ho'.format(vector[1][1], vector[1][0]))
#-----------------------------------
#START FREEZE
while START_FREEZE:
	sleep(100)

RESET_TIMER = 0
#-----------------------------------
#Bouncy bouncy balls
while True:
	RESET_TIMER = RESET_TIMER+1

	if(RESET_TIMER == REDRAW_WALL_PERIOD):
		for coordinates in borders:
			pp(f'\033[{coordinates[1]};{coordinates[0]}H\033[42m '.format(coordinates[1], coordinates[0]))
		pp(f'\033[{coordinates[1]};{coordinates[0]}H\u001b[0m'.format(1,1))
		RESET_TIMER = 0
			

	sleep(GRACE_PERIOD)
	for vector in vectors:
		head = vector[0]
		tail = vector[1]

		#Clear the head
		pp('\033[{};{}H '.format(head[1], head[0]))

		#Clear the tail
		if TAIL_TOGGLE:		
			pp('\033[{};{}H '.format(tail[1], tail[0]))

		#Calculate next move
		difference = [head[0] - tail[0], head[1] - tail[1]]
		
		#Shift the tail
		tail[0] = head[0]
		tail[1] = head[1]		
	
		#Shift the head
		head[0] = head[0] + difference[0]
		head[1] = head[1] + difference[1]
	
	#Collisions
	for vector in vectors:
		head = vector[0]
		tail = vector[1]
		for x in vectors:
			if x != vector:
				xHead = x[0]
				xTail = x[1]
				#Head on collision
				while xHead[0] == tail[0] and xHead[1] == tail[1] and xTail[0] == head[0] and xTail[1] == head[1]:
					#change the tails
					xTail[0], xTail[1] = xTail[0] + choice(MOVE_CHOICE), xTail[1] + choice(MOVE_CHOICE)
					tail[0],tail[1]  = tail[0] + choice(MOVE_CHOICE), tail[1] + choice(MOVE_CHOICE)
					
				#Merge collision
				if xHead[0] == head[0] and xHead[1] == head[1]:
					#change the tails
					xTail[0], xTail[1] = xTail[0] + choice(MOVE_CHOICE), xTail[1] + choice(MOVE_CHOICE)
					tail[0],tail[1]  = tail[0] + choice(MOVE_CHOICE), tail[1] + choice(MOVE_CHOICE)
					
	for vector in vectors:
		head = vector[0]
		tail = vector[1]
		wallBoop = [0,0]
		doBoop = False
		if head[0] == tail[0] and head[1] == tail[1]:
			offsetA, offsetB = 0,0
			while offsetA == 0 and offsetB == 0:
				offSetA, offsetB = choice(MOVE_CHOICE), choice(MOVE_CHOICE)
			tail[0] = tail[0] + offsetA
			tail[1] = tail[1] + offsetB
			
		if head[1] <= 1: #top wall
			doBoop = True
			head[1] = 2
			tail[1] = 1
			wallBoop[1] = 1
			if head[0] <= 1: #top left corner
				head[0] = 2
				tail[0] = 1
				wallBoop[0] = 1
			elif head[0] >= COLUMNS: #top right corner
				head[0] = COLUMNS - 1
				tail[0] = COLUMNS
				wallBoop[0] = COLUMNS
			else: #non corner
				tail[0] = head[0] + choice(MOVE_CHOICE)
				wallBoop[0] = head[0]

		elif head[1] >= ROWS: #bottom wall
			doBoop = True
			head[1] = ROWS - 1
			tail[1] = ROWS
			wallBoop[1] = ROWS
			if head[0] <= 1: #bottom left corner
				head[0] = 2
				tail[0] = 1
				wallBoop[0] = 1

			elif head[0] >= COLUMNS: #bottom right corner
				head[0] = COLUMNS - 1
				tail[0] = COLUMNS
				wallBoop[0] = COLUMNS

			else: #non corner
				tail[0] = head[0] + choice(MOVE_CHOICE)
				wallBoop[0] = head[0]

		elif head[0] <= 1 : #left wall
			doBoop = True
			head[0] = 2			
			tail[1], tail[0] = head[1] + choice(MOVE_CHOICE),1
			wallBoop[0] = 1
			wallBoop[1] = head[1]

		elif head[0] >= COLUMNS: #right wall
			doBoop = True
			head[0] = COLUMNS - 1
			tail[0], tail[1] = COLUMNS, head[1] + choice(MOVE_CHOICE)
			wallBoop[0] = COLUMNS
			wallBoop[1] = head[1]		

		#draw new molecule
		pp('\033[{};{}HO'.format(head[1], head[0]))
		if(doBoop):
			pp('\033[{};{}H\033[41m '.format(wallBoop[1], wallBoop[0]))
			pp(f'\033[{coordinates[1]};{coordinates[0]}H\u001b[0m'.format(1,1))
		if TAIL_TOGGLE:
			pp('\033[{};{}Ho'.format(tail[1], tail[0]))
	

